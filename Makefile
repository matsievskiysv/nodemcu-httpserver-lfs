######################################################################
# Makefile user configuration
######################################################################

# Path to nodemcu-uploader (https://github.com/kmpm/nodemcu-uploader)
NODEMCU-UPLOADER=../nodemcu-uploader/nodemcu-uploader.py

# Serial port
PORT=/dev/ttyUSB0
SPEED=115200

NODEMCU-COMMAND=$(NODEMCU-UPLOADER) -b $(SPEED) --start_baud $(SPEED) -p $(PORT) upload

######################################################################

HTTP_FILES := $(wildcard http/*)
LUA_FILES := $(wildcard *.lua)
LFS_IMAGE := $(wildcard lfs/*.img)

# Print usage
usage:
	@echo "make upload FILE:=<file>  to upload a specific file (i.e make upload FILE:=init.lua)"
	@echo "make upload_http          to upload files to be served"
	@echo "make upload_server        to upload the server code and init.lua"
	@echo "make upload_all           to upload all"
	@echo $(TEST)

# Upload one file only
upload:
	@python $(NODEMCU-COMMAND) $(FILE)

# Upload HTTP files only
upload_http: $(HTTP_FILES)
	@python $(NODEMCU-COMMAND) $(foreach f, $^, $(f))

# Upload spiffs files
upload_spiffs: $(LUA_FILES)
	@python $(NODEMCU-COMMAND) $(foreach f, $^, $(f))

# Upload lfs image
# Save LFS image name to lfs.lua
upload_lfs: $(LFS_FILES)
	@echo return \"$(LFS_IMAGE)\" > lfs.lua
	@python $(NODEMCU-COMMAND) $(LFS_IMAGE) lfs.lua
	@rm lfs.lua

# Upload all
upload_all: upload_http upload_spiffs upload_lfs
	@python $(NODEMCU-COMMAND) $(foreach f, $^, $(f))

.PHONY: upload_all upload_http upload_spiffs upload_lfs
